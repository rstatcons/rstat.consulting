# Source code of http://rstat.consulting website

```
bundle exec jekyll serve

# To generate website with posts from draft folder and to listen external connections
bundle exec jekyll serve --drafts --host 0.0.0.0

s3cmd sync --preserve --no-mime-magic --guess-mime-type _site/ s3://rstat.consulting

# To update jekyll
bundle update
```
