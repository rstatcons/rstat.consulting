---
layout: page
title: Training in data processing and analysis
excerpt: "RResults Consulting"
modified: 2016-05-30T09:39:10+0300 
---


# Our educational products

* From Excel to R
* Introduction to R
* Data Visualization with R
* Reproducible reports with R and Markdown
