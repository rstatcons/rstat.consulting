---
layout: page
title: Why we as a provider of R-related services
excerpt: "Rstat consulting"
modified: 2015-08-30T15:35:58+0200 
image:
  feature: so-simple-sample-image-4.jpg
  credit: WeGraphics
  creditlink: http://wegraphics.net/downloads/free-ultimate-blurred-background-pack/
---

1. We know how to listen to a client and to understand his or her core challenges.
2. We follow edge of progress and are aware of state of art methods in data science.
3. We can help you to motive, teach and train your team.

