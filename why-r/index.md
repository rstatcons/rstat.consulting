---
layout: page
title: Why R for a company
excerpt: "Rstat consulting"
modified: 2015-08-30T15:31:40+0200 
image:
  feature: so-simple-sample-image-4.jpg
  credit: WeGraphics
  creditlink: http://wegraphics.net/downloads/free-ultimate-blurred-background-pack/
---

If your company wants to support decision making by data insights on a regular basis you should adopt R.

A. R has widespread distribution: it is popular as in academia, so in business sector.

1. Diverse and active R community abundantly shares knowledge, experience and solutions related to R.
2. It is easier to find R-skilled employees, to get support for R-backed projects and not to get vendor lock-in.

B. R is an one-stop tool for full cycle of research.

1. 80% of time an analyst spends while extracting and preparing data for analysis. R provides connectivity to the most of existing types of data storage and has range of handy instruments to process data.
2. Due to popularity and open-source nature of R most of statistical, machine learning and visualization methods has their implementation in R.
