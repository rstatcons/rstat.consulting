---
layout: post
author: alex
title: "Brand new brands in Data Science"
modified:
categories: blog
excerpt: "Machine Learning, Data Mining, Deep Learning and Artificial Intelligence are in many cases the same sort of thing. These labels mark highly overlapping parts of data science. These labels are popular and heavily promoted on the market. "
tags: [statistics, machine learning, data science]
image: 
  feature: linear_regression_formula_400.jpg
date: 2017-10-24T17:08:15+0400 

---

On the 24th of October 2017 I took part in a seminar by [Vincenzo Lagani](http://www.gnosisda.gr/vincenzo-lagani/) hosted by [R-Ladies Tbilisi group](https://www.meetup.com/rladies-tbilisi/). Vincenzo gave broad introduction to machine learning with applications in R. 

Speaker covered philosophical, theoretical and practical questions of the topic. It looks like I was interested mostly in the first aspect. My findings are the following (it is totally my interpretation of Vincenzo's message mixed with my experience and I can be wrong in my conclusions). 

Machine Learning (ML), Data Mining (DM), Deep Learning (DL) and Artificial Intelligence (AI) are in many cases the same sort of thing. These labels mark highly overlapping parts of data science. These labels are popular and heavily promoted on the market. But they don't have standard definitions. 

Data Mining now is a fading star in mass media but it is the widest term that incorporates Machine Learning and friends. Data Mining includes analysis of a decision maker's problem, where Machine Learning is a process of choosing of a statistical model and its parameters for a given purpose and a data set. 

Intuitively Machine Learning differs from Statistical Learning by moving focus towards computation aspects of the process. In Statistical Learning we train our mathematical model to deal with our data set. Theoretically we can do it even with pen and paper. In Machine Learning we do the same task but employing computer power. 

Deep Learning is a subset of Machine Learning focused on Neural Networks and powered by vast usage of computational power. Fast computers with huge amount of memory allow Deep Learning to employ thousands of layers and to perform numerous iterations to choose best starting (hyper) parameters. Internally Neural Networks model consists of sets of transformed linear models. In Deep Learning we have myriads of them.

Where is Artificial Intelligence? Turn round, it is watching you! Sure, your autonomous car is observing. But the board computer simply feeds situational data to a statistical model trained under careful attention of Elon Mask's employees. 

