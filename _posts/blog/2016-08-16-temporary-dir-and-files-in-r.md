---
layout: post
author: alex
title: "When do you need temporary directory and files in R and how to use them"
modified:
categories: blog
excerpt: "Often user wants to save a file with data, to process it and then to remove it. R provides safe instruments for such tasks"
tags: [r]
comments: true
image:
  feature:
date: 2016-08-15T12:40:15+0300 
---

## When temporary files in R become handy

Every time you need to download a file from anywhere, to extract data from  it and then to throw it away you have to decide where to save this file. There could be many options, for example, to save in working directory or in home directory. It looks simple but you have to check whether a file with similar name already exists. Then you need to hold list of all temporary files to remove them later.

## How to use tempdir() and tempfile()

R provides you with special infrastructure for temp files. When you start a new R session a temporary directory is created. Location of the directory depends on your system and configuration. Function `tempdir()` returns path to the current temporary directory.

On GNU/Linux system its output could look like this:

{% highlight r %}
tempdir()
[1] "/tmp/RtmpQlfeCO"
{% endhighlight %}

On normal exit R [cleans up](http://stackoverflow.com/a/13567465) this directory, so don't put there nonreproducible data.

Now you know the place where to hold temporary stuff.

Next question is how to name temporary files. You can hardcode names in your script but again it is more convenient to use specialized R tool for it.

Function `tempfile()` generates file names what are _very likely to be unique among calls to ‘tempfile’ in an R session_.  The function takes three arguments: the file name pattern, the directory name and the file extension. By default you get a filename with pattern _file_ without extension located in per-session temporary directory.

{% highlight r %}
tempfile()
[1] "/tmp/RtmpQlfeCO/file1f0d1fd5e3ba"

tempfile(pattern = "", fileext = ".csv")
[1] "/tmp/RtmpQlfeCO/1f0d27e09a1c.csv"
{% endhighlight %}

You can use file names from `tempfile()` to save your temporary files. When you don't need them any more, you can delete them with `unlink()` R function.


