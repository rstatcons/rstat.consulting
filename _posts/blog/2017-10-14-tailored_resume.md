---
layout: post
author: alex
title: "Vacancy-optimized resume in automated way"
modified:
categories: blog
excerpt: "Applying to a vacancy you would like to customize your resume to requirements of the employer. Check this approach to automate the procedure."
comments: true
tags: [r, reports]
image: 
  feature: Curriculum_Vitae_by_DesiItaly_Flickr.jpg 
date: 2017-10-14T15:03:17+0400 

---

Once a colleague of mine told me he had sent out about one thousand resumes to
find a suitable job. I was really impressed and could not imagine how would I
complete a similar task. I usually tailor my resume to meet the requirements of
a specific position.

When you have diverse work experience you would like to prioritize selected
important achievement records and to withdraw irrelevant ones. If you have
overlapping job positions, due to part-time or project-based positions, you
might to have these positions as higher priority in the list.

## Ways of tailoring your resume

There are several approaches to tailor a resume of C.V. You could filter and
sort by job only, and leave all job content unedited. You could also keep the
complete list of your job history as is, but rearrange and preserve important
achievements inside each position. Alternatively you could select and alter the
content using both methods.

Additionally there is the time dimension.  Job experiences are usually of less
interest to employers as they are further back in time. It might be advantageous
to hide older records that are not related to the new position.

In my case almost every job position includes tasks from different spheres of
work. For example in Pskov Volny Institute I conducted sociological research
projects, organized alumni reunion events and lectured on measurement theory and
data analysis. If applying for a research position I would include records on
sociological projects and teaching experience but omit the alumni-relations tasks. 

So for a specific resume I want to rearrange overlapping or adjacent positions
to move the more important records higher in priority. For each position record,
I need to keep only relevant achievements and sort them from most to least relevant.

## How to implement automatic resume tailoring

### Keep relevant records only

We want to match specific experience records to a specific vacancy. A record can
be related to the vacancy or not. If it is related, we keep it, else we discard
it. In this case I retain research projects and lectures, but drop alumni.

To get this yes/no relationship, we introduce tags. Each achievement record is to be
marked with one or more tags. Sociological projects can have tags such as, among others: project
management, opinion poll, data analysis, research.

When I find an interesting vacancy I tag it also. The Research position tags
could get research, data analysis and presentation skills. Our algorithm should
look through the records and keep only those that have the same tags as the position.

### Order of records

Each related record has different value for the vacancy. With regard to research
position, my sociological projects are more important than lectures and
therefore should be higher in list than lectures.

To fulfill this requirement we estimate value of a record toward its each tag. The sociological projects record is twice or thrice more valuable in data science than my lectures (there were subjects on data analysis, yes). So for data analysis positions the first record gets three scores and the second record gets one score.

### What tags and values to use

I'm currently developing this technique and have used it only once with added
tags for one specific position application. I expect that with time and addition
of appropriate tags, my records will have sufficient range and quantity of tags
to cover a wider variety of possible vacancies.

Values for tags are relative to each other. You can define a default tag
value. For example, score 1. It means an achievement is entirely related to the
tag. If the achievement is partly associated with the tag you score it as 0.5
or 0.3. If the achievement exceeds average level you put 1.5, 2 or 3.  So very
highly related scores > 1, less related scores < 1. 

### What to do with time dimension

We usually arrange our job records in reverse chronological order. Usually it is
OK, but in some cases you might prefer to push some records up in priority. I
decided to combine both the value and age of my records. If a record is
relatively recent but has higher value for target vacancy it is placed before a
newer record with a lower score.

I've tested this approach once and it required a little iterative testing to
achieve good results.

## Application in R language

I implemented the approach described above in a set of scripts in the R
programming language. The source code is available in our GitLab account:
[R script resumer](https://gitlab.com/rstatcons/resumer). All technical details
are explained in the
[README file](https://gitlab.com/rstatcons/resumer/blob/master/README.md).

The structure consists of three parts:
[head](https://gitlab.com/rstatcons/resumer/blob/master/source/head.md), body
and
[tail](https://gitlab.com/rstatcons/resumer/blob/master/source/tail.md). Body
and tail are not modified by the algorithm and are presented as text files in
Markdown format.

Information with job experience is stored in a
[text file in YAML format](https://gitlab.com/rstatcons/resumer/blob/master/source/jobs.yaml). Each
job is described by title of the position, name of organization and period of
work. Also it can store an unlimited number of subrecords with achievements. Each
achievement has a description and a number of tags with associated values.

The script produces a final document in Markdown format. After manual check it
generates a PDF version file of the resume with Pandoc software.

_Twitter card image source: [Curriculum Vitae](https://www.flickr.com/photos/desiitaly/2304874364/) by Desi_
