---
layout: post
author: alex
title: "How to work with file system from R"
modified:
categories: blog
excerpt: "With R you can work with file system: retrieve names of files in specified directory, create, move and kill files and directories"
tags: [r]
comments: true
image:
  feature:
date: 2016-09-15T13:33:15+0300 
---

## Temporary directory and files

Firstly let's create a folder to work with in a temporary directory. Look for details on how to work with temporary files in R in my post [When do you need temporary directory and files in R and how to use them]({% post_url blog/2016-08-16-temporary-dir-and-files-in-r %}).

Function `tempfile()` does not create any files, it generates a random file name, what is unique for the current R session. By default `tempfile()` generates file name in R temporary directory.

{% highlight r %}
mydirname <- tempfile(pattern = "mydir")
mydirname
[1] "/tmp/RtmpVuBPUY/mydir1e5a69bc39ac"
{% endhighlight %}

## Exploring file system

In `mydirname` variable we have a file path where we can create a file. Now there is nothing. Let's check it with function `file.exists()` and the content of the temporary directory with function `dir()`.

{% highlight r %}
file.exists(mydirname)
[1] FALSE
dir(tempdir())
character(0) # Empty character vector
{% endhighlight %}

## Creating of a directory

Next we really touch our file system and create desired directory.

{% highlight r %}
dir.create(mydirname)
{% endhighlight %}

And check it again:

* existance;
* content of parent directory;
* modification time.

{% highlight r %}
file.exists(mydirname)
[1] TRUE
dir(tempdir(), full.names = TRUE)
[1] "/tmp/RtmpVuBPUY/mydir1e5a69bc39ac"
file.mtime(mydirname)
[1] "2016-08-17 19:19:37 MSK"

{% endhighlight %}

## More listings including R system files

Let's inspect files from one of installed R-packages. A command `system.file()` returns full path of files from installed packages. We use it to get a file path of `stats` package.

{% highlight r %}
system.file(package = "stats")
[1] "/usr/lib64/R/library/stats"
{% endhighlight %}

Next we list content of this folder.

{% highlight r %}
dir(system.file(package = "stats"))
 [1] "COPYRIGHTS.modreg" "demo"              "DESCRIPTION"    
 [4] "help"              "html"              "INDEX"          
 [7] "libs"              "Meta"              "NAMESPACE"      
[10] "R"                 "SOURCES.ts"     
{% endhighlight %}

We can check content of any subdir. For example "demo" folder.

{% highlight r %}
dir(system.file("demo", package = "stats"))
[1] "glm.vr.R" "lm.glm.R" "nlm.R"    "smooth.R"
{% endhighlight %}

In some situations you would prefer full names:

{% highlight r %}
dir(system.file("demo", package = "stats"), full.names = TRUE)
[1] "/usr/lib64/R/library/stats/demo/glm.vr.R"
[2] "/usr/lib64/R/library/stats/demo/lm.glm.R"
[3] "/usr/lib64/R/library/stats/demo/nlm.R"   
[4] "/usr/lib64/R/library/stats/demo/smooth.R"
{% endhighlight %}

## Constructing of file names in R 

What is a file name? It is a character string. Wrong. It is a system-specific character string. 

R function `file.path()` creates system-specific file names.

{% highlight r %}
workingdir <- "projects"
projectdir <- "warandpeace"
datadir    <- "data"
file.path(workingdir, projectdir, datadir)
# Linux-based OS
[1] "projects/warandpeace/data"
# Surprisingly in Windows we have similar results
[1] "projects/warandpeace/data"
{% endhighlight %}

### Note about Windows

Why did `file.path()` put slashes as separator, when we used to backslash in Windows (for example `C:\Program Files\R\R-3.3.1`)? There is even a special note on it in [`file.path`'s help page](https://stat.ethz.ch/R-manual/R-devel/library/base/html/file.path.html):

    The components are by default separated by ‘/’ (not ‘\’) on Windows.

Surprisingly [DOS and Windows supported slash](https://blogs.msdn.microsoft.com/larryosterman/2005/06/24/why-is-the-dos-path-character/) as file path separator from the beginning. So in most cases one can use as slash, so backslash in Windows.


