---
layout: post
author: alex
title: "Installation of Skype Call Recorder in Fedora"
modified:
categories: blog
excerpt: ""
tags: [skype, fedora, communication]
image:
  feature:
date: 2016-6-21T14:47:55+03:00
---

Recording Skype calls (when you do it legally) could be very useful for remote meetings, feedback collection, interviews. I tried to find a solution for this task and discovered good free option. [Skype Call Recorder](http://atdot.ch/scr/) is an open source software for recording Skype calls.

Developer [provides](http://atdot.ch/scr/download/) several binary packages for Debian-based GNU/Linux distributives. RPM-based packages are [dated](http://atdot.ch/scr/files/0.8/) 2009.

There are [instructions](http://atdot.ch/scr/compiling/) on compiling of Skype Call Recorder from source code. I have modified them to suit Fedora 23.
      	  
	  # install dependencies
	  sudo dnf install gcc-c++ make cmake qt-devel \ 
	  libabigail-devel fakeroot libvorbis-devel \ 
	  id3lib-devel lame-devel git 
	  
	  # retrieving source code 
	  git clone https://github.com/jlherren/skype-call-recorder.git
	  cd skype-call-recorder
	  
	  # compiling and installing
	  cmake .
	  make
	  sudo make install

Once installed, the software should appear in desktop menu and also be available in shell through `skype-call-recorder` command. You can read detailed description on how to use Skype Call Recorder on the respective [webpage](http://atdot.ch/scr/how-to-use/) of the developer.

And don't forget to abide by the laws involving matters of legal call recording.
