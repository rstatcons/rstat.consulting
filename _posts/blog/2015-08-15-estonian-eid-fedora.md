---
layout: post
author: alex
title: "How to install software for Estonian electronic ID card in Fedora Linux"
modified:
categories: blog
excerpt: ""
tags: [linux, fedora, esteid]
image:
  feature:
date: 2015-8-15T6:15:55+02:00
---

# Estonian e-residency

Estonia has one of the most advanced electronic governments. If not the most advanced. Its citizens can perform legal acts remotely via Internet: to vote, to register a company, to declare taxes, to sign a contract.

A citizen has a cryptographic keys, which are verified by Estonian state. Estonian governmental bodies have to accept requests of citizens not only in face-to-face or hard-copy ways, but also requests in form of digitally signed document.

As digitally signely signed documents have legal value, any two parties who have cryptographic keys, issued by Estonian government, could close a contract without face-to-face meetings and hard-copy documents.

Estonians decided to move on and started to issue such digital identity to anyone who wants. Of course, there is no any connection with Estonian citizenship. Estonia only confirms, that these cryptographic keys were issued to this person. It means document, signed by the keys, was signed by the person.

That is way, if you want to run a business at more global level, you can apply for Estonian "e-residency". It will allow you remotly to register company in Estonia, to open a bank account (currently this action requires one phisical visit to the bank), to sign contracts, to pay taxes and to get profit.


# Installation of required software in Fedora Linux

[ID.ee](http://www.id.ee/?lang=en&id= "I failed to find extended title of this organization"), the developer of E-residency software, provides packages only for Ubuntu linux. But Fedora repositories already contain required software.

So we need to install following components:

* system service PCSC to allow Linux communicate with smart-card reader
* driver to allow PCSC communicate with your specific smart-card reader
* plugins for web-browser to provide authentification and other operations with smart-card
* utility to manage PIN/PUN codes and certificates of smart-card.

Everything except driver could be installed with following command:

		sudo dnf install pcsc-lite firefox-esteid-plugin \
		firefox-esteidpkcs11loader qesteidutil qdigidoc

As about driver, along with the smart-card I got card reader 
[ACR38](http://www.acs.com.hk/en/products/4/acr38-smart-card-reader/). Drivers 
for the reader can be downloaded from the [official 
page](http://www.acs.com.hk/en/products/4/acr38-smart-card-reader/#tab_downloads) 
You need PC/SC Driver Package for Linux. It is a 2 mb zipped archive named like 
`ACS-Unified-PKG-Lnx-112-P.zip`.

		unzip -j ACS-Unified-PKG-Lnx-112-P.zip *fc23.i686.rpm
		sudo dnf install pcsc-lite-acsccid-1.1.2-1.fc23.i686.rpm

Check the version of the file in these commands.

Next you need to start PCSC system service. If you want the service to be automatically started at boot time, use the following command:

    sudo systemctl enable pcscd

The service will be started the next time you boot your Fedora. To start the service immediately use _start_ option:

    sudo systemctl start pcscd

# Using the software

Now you are able to work with your Estonian e-identity card in web-browser Mozilla Firefox. Link to ID-card utility is located Accessories menu. Also you can start it with command _qesteidutil_.

Look for further information at the [official website](https://e-estonia.com/e-residents/welcome/).

# Blinking of card-reader led

I found a problem with sequence of launching of Firefox and inserting of ACR38 card reader. If Firefox is already running and I insert the card with the card reader, led begins to blink chaotically and authorization does not work. 

To avoid this you should firstly insert card reader with ID card. Then launch or restart Firefox. In this case the led will be on and authorization will work.

