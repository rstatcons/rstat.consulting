---
layout: post
author: alex
title: "How to get with R a list of city streets from OpenStreetMap"
modified:
categories: blog
excerpt: ""
tags: [r, openstreetmap, overpass]
image:
  feature:
date: 2016-06-24T12:49:15+0300 

---

For building of a prototype of Point-of-Sales application we need to get list of streets names for a given city.