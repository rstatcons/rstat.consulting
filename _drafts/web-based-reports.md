---
layout: post
author: alex
title: "Time to drop static reports in favor of web-based interactive documents"
modified:
categories: blog
excerpt:
tags: [r, shiny, interactivity]
image:
  feature:
date: 2015-8-18T6:15:55+02:00

---

# It is time for interactive documents

For a long period of time decison-makers got data in form of static documents. From the beginning there were paper, aka hard-copy, reports and tables. Then we got computers, and organizations started to produce similar documents with help of informational technologies. Reports are almost identical to paper ancestry, except they are stored as digital files in formats of Microsoft Word or PDF. They are static and contain only those answers, which were asked by the author of the report.
Web is everywhere. 

Data is everywhere.

Clients know more.

Decisions have to be more reliable.
