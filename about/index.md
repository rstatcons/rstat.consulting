---
layout: page
title: Contacts
excerpt: "RResults Consulting OÜ"
modified: 2017-11-24T18:17:38
---

RResults Consulting OÜ  
Paju 20-36, Vöru, 65610 Estonia   

## Alexander Matrunich 

[a@rresults.consulting](mailto:a@rresults.consulting)   
+995555576954
Skype: amatrunitch

## Jim Maas

[jimmaasuk@gmail.com](mailto:jimmaasuk@gmail.com)   
+447719858698  
Skype: jamaas
